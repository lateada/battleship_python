Battleship

Battleship is a guessing game. For my version, Python randomly selects the row and column coordinates of the battleship on a 9 row by 9 column grid. The player tries to guess the location within five tries.
Only integers between 1 and 9 are accepted as valid guesses.
Hints are provided.

It’s fun – give it a try!!!