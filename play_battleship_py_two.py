"""FUN BATTLESHIP GAME SCRIPTED IN PYTHON TWOS
by la januari"""

from random import randint
def battle():
    board = []
#creating the board
    for x in range(9):
        board.append(["O"] * 9)

    def print_board(board):
        for row in board:
            print (" ".join(row))

    print ("Let's play Battleship! You have 5 chances" +
    " to sink my battleship!!!\n")
    print_board(board)

#row of the battleship to hit
    def random_row(board):
        return randint(0, len(board) - 1)

#col of the battleship to hit
    def random_col(board):
        return randint(0, len(board[0]) - 1)

    ship_row = random_row(board)
    ship_col = random_col(board)
###Test here:
    #print (ship_row + 1) #adjusts input row[0] is row[1]
    #print (ship_col + 1) #adjusts input col[0] is col[1]

    def play_again():
        question = raw_input("\n\nWould you like to play once more? y or n: ")
        text = question.lower()
        if text == "y" or text =="yes":
###Test here:
            #print (ship_row + 1) #adjusts input row[0] is row[1]
            #print (ship_col + 1) #adjusts input col[0] is col[1]
            battle()

        elif text == "n" or text == "no":
            print ("\nGoodbye! Thanks for playing!!!")
        else:
            print ("\nWell . . . Adios!")

#*************************************************************************#
#loop through 5 guesses
    for turn in range(4,-1,-1):

        print

        gr = raw_input("Guess Row Between 1 and 9...    Row: ")
        gc = raw_input("Guess Column Between 1 and 9... Column: ")

        if (gr.isdigit() == False) or (gc.isdigit() == False):
            print ("\nSorry, that is not a valid entry!\n")
            print ("\nYou have " + str(turn) + " turn(s) remaining!\n")

        elif (gr.isdigit() == True) or (gc.isdigit() == True):
            gr = int(gr)
            gc = int(gc)
            guess_row = gr -1
            guess_col = gc -1

            if guess_row == ship_row and guess_col == ship_col:
                print ("\nCongratulations! You sunk my battleship!")
                print()
                symbol = u'\u26C7'
                board[ship_row][ship_col]= symbol
                print_board(board) #prints board every turn
                play_again()
                break #ends the game

            if ((guess_row < 0 or guess_row > 8) or
                (guess_col < 0 or guess_col > 8)):
                print ("\nOops, that's not even in the ocean.")

            elif (board[guess_row][guess_col] == "X"):
                print ("\nYou guessed that one already.")

            elif guess_row != ship_row or guess_col != ship_col:
                print ("You missed my battleship!")
                board[guess_row][guess_col] = "X" #misses marked with an X

        ###hints
            if guess_row == ship_row and guess_col != ship_col:
                print ("    Good! Row is Correct!")
            elif guess_row != ship_row and guess_col == ship_col:
                print ("    Good! Column is Correct!")

            if (guess_row < ship_row):
                print ("    Hint:  Row is higher!")
            elif (guess_row > ship_row):
                print ("    Hint:  Row is lower!")

            if (guess_col < ship_col):
                print ("    Hint:  Column is higher!")
            elif (guess_col > ship_col):
                print ("    Hint:  Column is lower!")

            print ("\nYou have " + str(turn) + " turn(s) remaining!\n")

        if turn == 0:
            print ("\nGame Over!!!\n")
            print ("Row was: " + str(ship_row + 1))
            print ("Column was: " + str(ship_col + 1))
            print
            symbol = u'\u26F4'
###location revealed
            board[ship_row][ship_col]= symbol
            print_board(board)
            play_again()
            break
        print_board(board)


battle()


